#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>
#include <stdio.h>


struct image {
  uint64_t width, height;
  struct pixel* pixels;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image image_create(uint64_t width, uint64_t height);

void image_destroy(struct image* image);


uint64_t get_height(const struct image* image);
uint64_t get_width(const struct image* image);

#endif


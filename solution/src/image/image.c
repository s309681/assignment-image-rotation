#include "image.h"

struct image image_create(uint64_t width, uint64_t height) {
  struct image image = {0};
  image.width = width;
  image.height = height;
  image.pixels = malloc(sizeof(struct pixel) * height * width);
  return image;
}

void image_destroy(struct image *image) {
  free(image->pixels);
}

size_t get_height(const struct image *image) {
    return image->height;
}

size_t get_width(const struct image *image) {
    return image->width;
}


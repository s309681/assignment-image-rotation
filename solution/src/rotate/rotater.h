#ifndef ROTATER_H
#define ROTATER_H

#include "../image/image.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct point {
    size_t i, j;
};

struct image rotate_image(struct image *original);

struct pixel* image_pixel_at( struct image img, struct point p );
struct point rotate_preimage( struct image img, struct point p );

#endif


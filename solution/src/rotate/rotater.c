#include "rotater.h"

struct pixel *image_pixel_at(struct image img, struct point p) {
    return (img.pixels + (img.width * p.i + p.j));
}

struct point rotate_preimage(struct image img, struct point p) {
    return (struct point) {((img.width - p.j - 1) * img.height + p.i) / img.height,
                           ((img.width - p.j - 1) * img.height + p.i) % img.height};
}

struct image rotate_image(struct image *original) {
    //делаем реверсию высоты и ширины
    struct image rotated = image_create(original->height, original->width);
    for (size_t i = 0; i < rotated.height; i++) {
        for (size_t j = 0; j < rotated.width; j++) {
            const struct point p = {i, j};
            *image_pixel_at(rotated, p) = *image_pixel_at(*original, rotate_preimage(rotated, p));
        }
    }
    return rotated;
}


#include "bmp.h"


#define DECLARE_FIELD(type, name) type name;

#undef FOR_HEADER
#undef DECLARE_FIELD

static const uint16_t BMP_FILE_SIGNATURE = 0x4d42;
static const uint32_t HEADER_INFO_SIZE = 40;
static const uint16_t BITS_PER_PIXEL = 24;

struct __attribute__((packed)) header {
    uint16_t signature;
    uint32_t file_size;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits_per_pixel;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_pixels_per_m;
    uint32_t y_pixels_per_m;
    uint32_t colors_used;
    uint32_t important_colors;
};

static uint32_t get_padding(uint32_t width) { return width % 4; }

static struct header generate_header(uint32_t width, uint32_t height) {
    const uint32_t head_size = sizeof(struct header);
    const uint32_t img_size = sizeof(struct pixel)
                              * height * (width + get_padding(width));
    const uint32_t file_size = head_size + img_size;
    return (struct header) {
            .signature = BMP_FILE_SIGNATURE,
            .file_size = file_size,
            .reserved = 0,
            .data_offset = sizeof(struct header),
            .header_size = HEADER_INFO_SIZE,
            .width = width,
            .height = height,
            .planes = 1,
            .bits_per_pixel = BITS_PER_PIXEL,
            .compression = 0,
            .image_size = 0,
            .x_pixels_per_m = 0,
            .y_pixels_per_m = 0,
            .colors_used = 0,
            .important_colors = 0
    };
}

enum read_status read_header(FILE *in, struct header *header) {
    rewind(in);
    if (fread(header, sizeof(struct header), 1, in) != 1) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status read_image(FILE *in, struct image *img) {
    for (uint32_t i = 0; i < img->height; i++) {
        if (fread(&img->pixels[i * img->width], sizeof(struct pixel), img->width, in)
            < img->width || fseek(in, get_padding(img->width), SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct header header = {0};
    {
        enum read_status status = read_header(in, &header);
        if (status != READ_OK) {
            return status;
        }
    }
    *img = image_create(header.width, header.height);
    {
        enum read_status status = read_image(in, img);
        if (status != READ_OK) {
            return status;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    const struct header header = generate_header(img->width, img->height);
    if (fwrite(&header, sizeof(struct header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    for (uint32_t o = 0, i = 0; i < img->height; i++) {
        if (fwrite(&img->pixels[i * img->width], sizeof(struct pixel), img->width, out)
            < header.width || fwrite(&o, get_padding(img->width), 1, out) < 1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

